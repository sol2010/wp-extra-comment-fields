<?php
/* An abstract class for providing list based options */
abstract class ECF_Option_List extends ECF_Option {

	public function options_form_field( $ref, $field = null ) {
		return sprintf( '<input type="text" class="large-text" name="%s[%s]" '
				. 'value="%s" />', $this->get_name(), $ref,
				implode( ', ', $this->get_value( $field ) ) );
	}

	public function options_form_field_placeholder( $field = null ) {
		return sprintf( '<input type="text" class="large-text" '
			. 'value="%s" disabled="disabled" />',
			implode( ', ', $this->get_value( $field ) ) );
	}

	public function get_value( $field ) {
		return $field ? preg_split( '/, ?/', parent::get_value( $field ) )
			: $this->get_default_value();
	}
}
?>