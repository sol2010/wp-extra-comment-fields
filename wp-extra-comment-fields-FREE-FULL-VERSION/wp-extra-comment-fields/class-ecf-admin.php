<?php
require_once 'class-ecf-admin-header.php';
require_once 'class-ecf-feature-pointer.php';

class ECF_Admin {
	// The name of the plugin's settings
	const GLOBAL_SETTINGS_PAGE = 'ecf-settings';
	// The name of the field options page
	const FIELD_OPTIONS_PAGE = 'ecf-field-options';

	// A feature pointer to field options
	private $field_options_feature_pointer;

	
	// Constructor initialising hooks and filters
	public function __construct() {
		add_action( 'admin_menu', array( &$this, 'menu' ) );
		add_filter( 'plugin_action_links',
			array( &$this, 'add_settings_links' ), 10, 2 );
	/*	if ( isset( $GLOBALS['ecf_plugin_API'] )
			&& $GLOBALS['ecf_plugin_API']->get_deliverables() ) {
			$this->add_field_options_feature_pointer();
		} */
	}

	/* Add a feature pointer for field options */
	public function add_field_options_feature_pointer() {
		$this->field_options_feature_pointer = new ECF_Feature_Pointer(
			'ecf-field-options', '#menu-comments',
			'Add your Extra Comments Fields',
			'<p>You can use the Extra Comment Fields page to add new Extra '
			. 'Comment Fields to the comment form and specify when and how '
			. 'they are displayed.</p>' );
	}

	public function menu() {
		/*if ( isset( $GLOBALS['ecf_plugin_API'] )
			&& $GLOBALS['ecf_plugin_API']->get_deliverables() ) { 
			*/
			add_comments_page( ECF_Main::PLUGIN_NAME, ECF_Main::PLUGIN_NAME, 3,
				self::FIELD_OPTIONS_PAGE, array( &$this, 'options_page' ) );
		// } 
		
		$ecf_settings = ECF_Main::PLUGIN_NAME . ' Settings';
		add_options_page( $ecf_settings, $ecf_settings, 3,
			self::GLOBAL_SETTINGS_PAGE,
			array( &$this, 'global_settings_page' ) );
	}

	// Add settings link to plugin list
	// Based on: http://www.whypad.com/posts/wordpress-add-settings-link-to-plugins-page/785/
	public function add_settings_links( $links, $file ) {
		$link = '<a href="%s.php?page=%s">%s</a>';
		$field_options_link = sprintf( $link, 'edit-comments',
			self::FIELD_OPTIONS_PAGE, __( 'Fields' ) );
		$global_settings_link = sprintf( $link, 'options-general',
			self::GLOBAL_SETTINGS_PAGE, __( 'Settings' ) );
		if ( preg_match( '/\/ecf.php$/', $file ) ) {
			// if ( $GLOBALS['ecf_plugin_API']->get_deliverables() )
				array_unshift( $links, $field_options_link );
			array_unshift( $links, $global_settings_link );
		}
		return $links;
	}

	/* Plugin wide settings including licensing */
	public function global_settings_page() {
?>
		<div class="wrap" style="max-width: 950px !important;">
			<div id="icon-options-general" class="icon32"></div>
			<h2>WP Extra Comment Fields</h2>
			<?php settings_errors(); ?>
			<?php
		/* Display admin header */
		$admin_header = new ECF_Admin_Header();
		$admin_header->display();
?>
			<form method="post" action="options.php">
				<?php settings_fields( self::GLOBAL_SETTINGS_PAGE ); ?>
				<?php do_settings_sections( self::GLOBAL_SETTINGS_PAGE ); ?>
				<?php submit_button(); ?>
			</form>
		</div>
		<?php
	}

	/* Display the plugin's settings page */
	public function options_page() {
		global $ecfdb;

		// Info for form
		$actionurl = $_SERVER['REQUEST_URI'];
?>
		<div class="wrap" style="max-width: 950px !important;">
		<div id="icon-edit-comments" class="icon32"></div>
		<h2>WP Extra Comment Fields</h2>

		<?php
		/* Delete fields if necessary */
		if ( array_key_exists( 'delete', $_GET ) )
			$ecfdb->delete_field( $ecfdb->unwrap_id( $_GET['delete'] ) );
?>

		<?php
		/* Call post and display any messages if the form has been submitted */
		if ( array_key_exists( 'submitted' , $_POST ) )
			$this->options_page_post();
?>

		<?php
		/* Display admin header */
		$admin_header = new ECF_Admin_Header();
		$admin_header->display();
?>

		<?php
		/* Display an editable list of fields */
		$name = ECF_Option::get_option( 'name' );
		$type = ECF_Option::get_option( 'type' );
		$public = ECF_Option::get_option( 'public' );
		$required = ECF_Option::get_option( 'required' );
?>
		<form name="ecf-field-table" action="<?php echo $actionurl ?>"
			method="post">
			<input type="hidden" name="submitted" value="table" />
			<?php wp_nonce_field( 'wp-extra-comment-fields' ); ?>
			<h3>
				Fields
				<span style="float : right;">
					<input type="submit" class="button-primary"
						value="Save Changes" />
				</span>
			</h3>
			<table class="widefat">
				<thead>
					<tr>
						<th scope="col" class="manage-column">
							<?php echo $name->get_full_name(); ?>
						</th>
						<th scope="col" class="manage-column">
							<?php echo $type->get_full_name(); ?>
						</td>
						<th scope="col" class="manage-column">
							<?php echo $public->get_full_name(); ?>
						</td>
						<th scope="col" class="manage-column">
							<?php echo $required->get_full_name(); ?>
						</td>
						<th scope="col" class="manage-column">
						</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach ( $ecfdb->get_fields_by_ref( false )
			as $ref => $field ) {
			$editing = isset( $_GET['edit'] )
				&& $_GET['edit'] === $ref;
			$class = $editing ? ' class="editing" ' : '';
?>
						<tr<?php echo $class; ?>>
							<th scope="row">
								<?php echo $name->get_value( $field ); ?>
								<?php if ( $editing ) { ?>
								 <em><strong>(editing)</strong></em>
								<?php } ?>
							</th>
							<td>
								<?php echo $type->options_form_field_or_placeholder( $ref,
				$field ); ?>
							</td>
							<td>
								<?php echo $public->options_form_field_or_placeholder( $ref,
				$field ); ?>
							</td>
							<td>
								<?php echo $required->options_form_field_or_placeholder( $ref,
				$field ); ?>
							</td>
							<td>
								<a name="edit"
									href="?page=<?php echo self::FIELD_OPTIONS_PAGE; ?>&amp;edit=<?php echo $ref; ?>">
									<span class="button-secondary"
										style="vertical-align: bottom; margin-right: 2px;">
										Edit
									</span>
								</a>
								<a name="delete"
									href="?page=<?php echo self::FIELD_OPTIONS_PAGE; ?>&amp;delete=<?php echo $ref; ?>"
									onclick="return confirm('Are you sure you want to delete this field?');">
									<span class="button-secondary"
										style="vertical-align: bottom; margin-left:2px;">
										Delete
									</span>
								</a>
							</td>
						</tr>
					<?php } ?>
					<tr>
						<td>
							<?php echo $name->options_form_field_or_placeholder( 'ecf_new' ); ?>
						</td>
						<td>
							<?php echo $type->options_form_field_or_placeholder( 'ecf_new' ); ?>
						</td>
						<td>
							<?php echo $public->options_form_field_or_placeholder( 'ecf_new' ); ?>
						</td>
						<td>
							<?php echo $required->options_form_field_or_placeholder( 'ecf_new' ); ?>
						</td>
						<td>
							<input type="submit" name="submit" id="add"
								class="button-primary" value="Add" />
						</td>
					</tr>
				</tbody>
			</table>
		</form>

		<script type="text/javascript">
			/* Correctly handle adding new fields via the enter key */
			jQuery('input[type="text"][name="name[ecf_new]"]').keyup(function(e) {
				if (13 == e.keyCode)
					jQuery('#add').click();
				return false;
			});
		</script>

		<?php
		/* Display the editable option of the current field */
		if ( array_key_exists( 'edit' , $_GET ) ) {
			$ref = $_GET['edit'];
			$field = $ecfdb->get_field_by_ref( $ref );
?>
		<form name="ecf_field_options" action="<?php echo $actionurl; ?>"
			method="post">
			<input type="hidden" name="submitted" value="options" />
			<?php wp_nonce_field( 'wp-extra-comment-fields' ); ?>
			<h3>
				Options
				<span style="float : right;">
					<input type="submit" class="button-primary"
						value="Save Changes" />
				</span>
			</h3>
			<table class="form-table">
				<tbody>
					<?php foreach ( ECF_Option::get_applicable_options( $field )
				as $option ) { ?>
					<tr valign="top">
						<th scope="row">
							<label for="<?php echo $option->get_name(); ?>">
								<?php echo $option->get_full_name(); ?>
							</label>
						</th>
						<td>
							<?php echo $option->options_form_field_or_placeholder( $ref, $field ); ?>
							<span class="description">
								<?php echo $option->get_description(); ?>
							</span>
							<?php
				// Display an apropriate message if this option is
				// not available
				if ( ! $option->is_available() )
					ECF_Admin_Notifications::unavailable(
						$option->available_in() );
?>
						</td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
		</form>
		<?php } ?>

		<?php
	}

	/* Update fields based on the options page */
	public function options_page_post() {
		global $ecfdb;

		check_admin_referer( 'wp-extra-comment-fields' );

		if ( 'Add' == $_POST['submit'] ) {
			$ecfdb->add_field( array( 'name' => $_POST['name']['ecf_new'],
					'type' => $_POST['type']['ecf_new'],
					'required' => $_POST['required']['ecf_new'],
					'public' => $_POST['public']['ecf_new'] ) );
		} else {
			$ref = array_key_exists( 'edit',  $_GET ) ? $_GET['edit'] : null;
			foreach ( ECF_Option::get_options() as $option ) {
				if ( 'options' == $_POST['submitted'] )
					$option->options_form_post_single( $ref,
						$ecfdb->get_field_by_ref( $ref ) );
				else if ( 'table' == $_POST['submitted'] )
					$option->options_form_post_all();
			}
		}

		$this->success_message( "Options successfully updated." );
	}

	private function success_message( $message ) {
?>
		<div class="updated">
			<p>
				<?php echo $message; ?>
			</p>
		</div>
		<?php
	}
}
?>
