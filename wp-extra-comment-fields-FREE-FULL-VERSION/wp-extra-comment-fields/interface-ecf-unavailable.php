<?php
interface ECF_Unavailable {
	/* The version of the plugin in which this option is available */
	public function available_in();
}
?>