<?php
class ECF_Mail {
	public function __construct() {
		add_filter( 'comment_moderation_text',
			array( &$this, 'notify_moderator' ), 99, 2 );
		add_filter( 'comment_notification_text',
			array( &$this, 'notify_moderator' ), 99, 2 );
	}

	public function notify_moderator( $message, $comment_id ) {
		global $ecfdb;

		$comment_text = get_comment_text( $comment_id );
		$data = $ecfdb->get_comment_data( $comment_id );

		# $fieldtext = "Extra Comment Fields:\n";
		$field_texts = array();
		foreach ( $data as $id => $datum ) {
			$field = $ecfdb->get_field( $id );
			if ( ECF_Option::get_option( 'email' )->get_value( $field ) ) {
				$field_type = ECF_field_type::get_type( $field->type );
				$field_texts[] = $field_type->display_plaintext_field(
					ECF_Option::get_option( 'name' )->get_value( $field ),
					$datum );
			}
		}
		$field_text = implode( "\n" , $field_texts );

		return str_replace( "$comment_text", "$comment_text\n\n$field_text",
			$message );
	}
}
?>