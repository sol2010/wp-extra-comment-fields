<?php
/**
 * A class containing various methods for displaying notifications
 */
class ECF_Admin_Notifications {
	function __construct() {
	}

	public static function message_updated( $message, $classes = null ) {
		self::updated( sprintf( '<p><strong>%s</strong></p>', $message ),
			(array)$classes );
	}

	public static function updated( $html, $classes = null ) {
		self::notification( $html,
			array_merge( (array)'updated', (array)$classes ) );
	}

	public static function message_alert( $message, $classes = null ) {
		self::alert( sprintf( '<p><strong>%s</strong></p>', $message ),
			(array)$classes );
	}

	public static function alert( $html, $classes = null ) {
		self::notification( $html,
			array_merge( (array)'error', (array)$classes ) );
	}

	public static function notification( $html, $classes = null ) {
		printf( '<div class="%s">%s</div>', implode( ' ',
				array_merge( (array)'ecf-notification', (array)$classes ) ),
			$html );
	}

	public static function unavailable( $available_in = '' ) {
		$edition = sprintf( 'ecf-edition-%s',
			strtolower( $available_in ) );
		$msg = sprintf( 'This is not available in your edition of the plugin. '
			. 'In order to acess it and many other great features, please '
			. 'upgrade to <span class="edition">%s</span> or above.',
			$available_in );
		self::notification( $msg, array( 'unavailable', $edition ) );
	}
}
?>
