<?php
/* An abstract class for providing checkbox options */
abstract class ECF_Option_Boolean extends ECF_Option {

	public function options_form_field( $ref, $field = null ) {
		return sprintf( '<input type="checkbox" name="%s[%s]" value="1" %s />',
			$this->get_name(), $ref,
			checked( $this->get_value( $field ), true, false ) );
	}

	public function options_form_field_placeholder( $field = null ) {
		return sprintf( '<input type="checkbox" %s disabled="disabled" />',
			checked( $this->get_value( $field ), true, false ) );	
	}

	/* Options form post for a single field */
	public function options_form_post_single( $ref, $field ) {
		$this->update_value( $field,
			isset( $_POST[$this->get_name()][$ref] )
			? $_POST[$this->get_name()][$ref] : false );
	}

	public function update_value( $field, $value ) {
		parent::update_value( $field, intval( $value ) );
	}

	public function sql_definition() {
		return sprintf( '`%s` boolean NOT NULL DEFAULT %s',
			$this->get_name(), intval( $this->get_default_value() ) );
	}
}
?>