<?php
/* An abstract class for providing options */
abstract class ECF_Option {
	private static $options = array();
	private $field_types = array();

	/* Constructor */
	public function __construct( $field_types = null ) {
		require_once( 'interface-ecf-visibility-condition.php' );
		$this->field_types = $field_types ? $field_types
			: array_keys( ECF_Field_Type::get_types() );
		self::register_option( $this );
	}

	/* Should the option be displayed to a given field */
	public function is_applicable( $field ) {
		return in_array( $field->type, $this->field_types );
	}

	/* Add and option to the list of options */
	protected static function register_option( $option ) {
		self::$options[$option->get_name()] = $option;
	}

	/* Return the appropriate option */
	public static function get_option( $name ) {
		return self::$options[$name];
	}

	/* Return all options */
	public static function get_options() {
		$options = self::$options;
		uasort( $options, 'ecf_prioritycmp' );
		return $options;
	}

	/* Return all options which apply to a given field */
	public static function get_applicable_options( $field ) {
		$options = array();
		foreach ( self::get_options() as $name => $option ) {
			if ( $option->is_applicable( $field ) )
				$options[$name] = $option;
		}
		return $options;
	}

	/* Return all visibility conditions */
	public static function get_visibility_conditions() {
		$conditions = array();
		foreach( self::get_options() as $name => $option ) {
			if ( $option instanceof ECF_Visibility_Condition )
				$conditions[$name] = $option;
		}
		return $conditions;
	}

	/* Are all visibility conditions satisfied? */
	public static function are_visibility_conditions_satisfied( $field ) {
		$all = true;
		foreach ( self::get_visibility_conditions() as $condition ) {
			$all &= $condition->is_satisfied( $field );
		}
		return $all;
	}

	/* Get the name of the option as it is to be displayed */
	public abstract function get_full_name();

	/* Get the database field in which this option is stored */
	public abstract function get_name();

	/* Get the types of the fields to which this option applies */
	public function get_field_types() {
		return $field_types;
	}

	/* Get a description of the field */
	public abstract function get_description();

	/* Options form field */
	public abstract function options_form_field( $ref, $field = null );

	/* Placeholder for when the field is not available */
	public function options_form_field_placeholder( $field = null ) {
		return "<span class='default_placeholder'>Option unavailable</span>";
	}

	/* Either the options form field or the placeholder if the field is not
	available */
	public function options_form_field_or_placeholder( $ref, $field = null ) {
		return $this->is_available()
			? $this->options_form_field( $ref, $field )
			: $this->options_form_field_placeholder( $field );
	}

	/* Options form post for all fields */
	public function options_form_post_all() {
		global $ecfdb;

		if ( is_array( $_POST[$this->get_name()] )
			&& array_key_exists( 'ecf_new', $_POST[$this->get_name()] ) )
			unset( $_POST[$this->get_name()]['ecf_new'] );
		foreach ( $ecfdb->get_fields_by_ref() as $ref => $field ) {
			$this->options_form_post_single( $ref, $field );
		}
	}

	/* Options form post for a single field */
	public function options_form_post_single( $ref, $field ) {
		if ( isset( $_POST[$this->get_name()][$ref] ) )
			$this->update_value( $field, $_POST[$this->get_name()][$ref] );
	}

	/* Default setting */
	public abstract function get_default_value();

	/* Retrieve the current value from a field */
	public function get_value( $field ) {
		$name = $this->get_name();
		return $field ? $field->$name : $this->get_default_value();
	}

	/* Retriever the current value from the db */
	public function get_value_by_id( $id ) {
		global $ecfdb;

		return $this->get_value( $ecfdb->get_field( $id ) );
	}

	/* Update the value for the option stored in the database */
	public function update_value( $field, $value ) {
		global $ecfdb;

		$ecfdb->update_field( $field->ID,
			array( $this->get_name() => $value ) );
	}

	/* The sql necessary to define the name and properites of the db field */
	public function sql_definition() {
		return sprintf( '`%s` varchar(255) NOT NULL DEFAULT \'%s\'',
			$this->get_name(), $this->get_default_value() );
	}

	/* This determines how high up the page the option is displayed */
	public function priority() {
		return 0;
	}

	/* Is the option available in the current version of the plugin? */
	public function is_available() {
		return ! ( $this instanceof ECF_Unavailable );
	}
}

/* Compare based on priority */
function ecf_prioritycmp( $a, $b ) {
	$pa = $a->priority();
	$pb = $b->priority();
	return $pa < $pb ? 1 : ( $pa > $pb ? -1 : 0 );
}
?>