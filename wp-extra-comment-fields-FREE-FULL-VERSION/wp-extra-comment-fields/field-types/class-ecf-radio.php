<?php
/* Radio form */
class ECF_Radio extends ECF_Field_Type {
	protected $name = 'radio';

	public function form_field( $ref, $field ) {
		global $ecfdb;
		$name = $ecfdb->html_string( $field->name );
		echo "<div class='ecf-form-field-title'>$name</div>\n";
		$values = preg_split( '/, ?/', $field->values );
		echo '<div class=\'ecf-form-field-input\'>';
		foreach ( $values as $i => $value ) {
			$checked = checked( $i, 1, false );
			echo "<p><label><input type='radio' name='$ref' "
				. "value='$value'$checked /> $value</p></label>\n";
		}
		echo '</div>';
	}

	public function get_description() {
		return "Radio selection field";
	}
}

new ECF_Radio();
?>
