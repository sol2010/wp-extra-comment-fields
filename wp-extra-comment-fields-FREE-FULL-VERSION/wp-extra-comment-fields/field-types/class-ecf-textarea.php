<?php
/* Text form */
class ECF_Textarea extends ECF_Field_Type {
	protected $name = 'textarea';

	public function form_field( $ref, $field ) {
		global $ecfdb;
		$name = $ecfdb->html_string( $field->name );
		echo "<p class='comment-form-author'>\n";
		echo "<label class='ecf-form-field-title' for='$ref'>$name</label>\n";
		$req = ECF_Option::get_option( 'required' )->get_value( $field );
		$required = $req ? 'required' : '';
		$aria_required = $req ? 'true' : 'false';
		$rows = $field->length && $field->length > 0
			? "rows='$field->length'" : '';
		if ( $req )
			echo "<span class='required'>*</span>\n";
		echo "<textarea class='ecf-form-field-input' name='$ref' "
			. "id='$ref' $required $rows "
			. "aria-required='$aria_required'></textarea>\n";
		echo "</p>";
	}

	public function display_field( $id, $name, $value ) {
		return "<div class='ecf-field ecf-field-$id'>"
			. "<strong class='ecf-question'>$name:</strong>"
			. "<div class='ecf-response'>$value</div></div>";
	}

	public function display_plaintext_field( $name, $value ) {
		return "$name:\n$value\n";
	}

	public function get_description() {
		return "Textarea field";
	}
}

new ECF_Textarea();
?>
