<?php
/* An abstract class for providing integeral options */
abstract class ECF_Option_Integer extends ECF_Option {

	public function options_form_field( $ref, $field = null ) {
		return sprintf( '<input type="text" class="small-text" name="%s[%s]" '
				. 'value="%s" />', $this->get_name(), $ref,
				$this->get_value( $field ) );
	}

	public function options_form_field_placeholder( $field = null ) {
		return sprintf( '<input type="text" class="small-text" value="%s"
			disabled="disabled" />',
			$this->get_value( $field ) );
	}

	public function update_value( $field, $value ) {
		parent::update_value( $field, intval( $value ) );
	}

	public function sql_definition() {
		return sprintf( '`%s` int(11) NOT NULL DEFAULT %d',
			$this->get_name(), $this->get_default_value() );
	}
}
?>