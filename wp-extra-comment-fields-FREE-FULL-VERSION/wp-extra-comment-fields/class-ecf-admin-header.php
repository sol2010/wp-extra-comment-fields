<?php
/**
* Class for dislaying a header containing plugin information
*/
class ECF_Admin_Header {
	private $data;

	function __construct( $data = null )
	{
		$this->data = is_array( $data ) ? $data : ecf_plugin_data();
	}

	public function links_html() {
		$links = array();
		foreach ( $this->data['links'] as $text => $url ) {
			$links[] = sprintf( '<a href="%s" target="_blank">%s</a>', $url,
				$text );
		}
		return $links;
	}

	public function website_link_html() {
		$url = $this->data['website'];
		return sprintf( '<a href="%s" target="_blank">%s</a>', $url,
			parse_url( $url, PHP_URL_HOST ) );
	}

	public static function syndetic_list( $items ) {
		$values = array_values( $items );
		$n = count( $values );
		switch ( $n ) {
			case 0: return '';
			case 1: return $values[0];
			default: return implode( ', ', array_slice( $values, 0, $n - 1 ) )
				. ' and ' . $values[$n - 1];
		}
	}

	public function display_field( $class, $label, $value ) {
		printf( '<div class="%s"><span class="property-label">%s</span>%s'
			. '</div>', $class, $label, $value );
	}

	public function display() {
		echo '<div class="updated ecf-admin-header">';
		$this->display_field( 'warning', '', $this->data['warning'] );	
		$this->display_field( 'version', 'Version', $this->data['version'] );
		$this->display_field( 'edition', 'Edition', $this->data['edition'] );
		$this->display_field( 'website', 'Plugin Website',
			$this->website_link_html() );
		$this->display_field( 'authors', 'Written by',
			self::syndetic_list( $this->data['authors'] ) );
		$this->display_field( 'links', 'Links',
		implode( ' ', $this->links_html() ) );
		
		echo '</div>';
	}
}
?>