<?php
class ECF_Option_Category extends ECF_Option_Integer
	implements ECF_Visibility_Condition {
	
	public function get_name() {
		return 'category';
	}

	public function get_full_name() {
		return 'Category';
	}

	public function get_description() {
		return 'The category for which you wish the field to be displayed.';
	}

	public function get_default_value() {
		return -1;
	}

	public function options_form_field( $ref, $field = null ) {
		$args = array(
			'name' => sprintf( '%s[%s]', $this->get_name(), $ref ),
			'hide_empty' => 0,
			'selected' => $this->get_value( $field ),
			'orderby' => 'name',
			'hierarchical' => true,
			'hide_empty' => false,
			'show_option_none' => 'All',
			'echo' => 0
		);
		return wp_dropdown_categories( $args );
	}

	private static function category_exists( $term_id ) {
		foreach ( get_the_category() as $category ) {
			if ( $term_id == $category->term_id )
				return true;
		}

		return false;
	}

	public function is_satisfied( $field ) {
		$term_id = $this->get_value( $field );
		return !is_numeric( $term_id ) || -1 == $term_id
			|| $this->category_exists( $term_id );
	}

	public function priority() {
		return 60;
	}
}
new ECF_Option_Category();
?>