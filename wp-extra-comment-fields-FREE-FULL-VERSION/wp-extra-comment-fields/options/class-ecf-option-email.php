<?php
class ECF_Option_EMail extends ECF_Option_Boolean {
	
	public function get_name() {
		return 'email';
	}

	public function get_full_name() {
		return "Include in Admin E-Mails";
	}

	public function get_description() {
		return 'Enable this for the field to be displayed in admin '
			. 'notification emails.';
	}

	public function get_default_value() {
		return true;
	}

	public function priority() {
		return 76;
	}
}
new ECF_Option_EMail();
?>