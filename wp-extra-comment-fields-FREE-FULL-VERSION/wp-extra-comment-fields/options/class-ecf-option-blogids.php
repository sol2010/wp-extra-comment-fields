<?php
class ECF_Option_Blogids extends ECF_Option_List
	implements ECF_Visibility_Condition {
	
	public function get_name() {
		return 'multisite_blogids';
	}

	public function get_full_name() {
		return 'Blog IDs';
	}

	public function get_description() {
		return 'Which blogs do you want to display this field on? (This '
			. ' option is spectific to WordPress\'s multisite feature).';
	}

	public function get_default_value() {
		return '';
	}

	public function is_satisfied( $field ) {
		global $blogid;

		$blogids = $this->get_value( $field );
		// The blog id must be in the array of valid blog ids if $blogid is
		// defined
		return null == $blogid || '' == $blogids[0]
			|| in_array( $blogid, $blogids );
	}

	public function priority() {
		return 50;
	}
}
new ECF_Option_Blogids();
?>