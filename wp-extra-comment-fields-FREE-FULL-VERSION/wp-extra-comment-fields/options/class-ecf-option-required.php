<?php
class ECF_Option_Required extends ECF_Option_Boolean {
	
	public function get_name() {
		return 'required';
	}

	public function get_full_name() {
		return "Required";
	}

	public function get_description() {
		return 'This field must be completed in order to submit a comment '
			. '(e.g. like the email field).';
	}

	public function get_default_value() {
		return false;
	}

	public function priority() {
		return 78;
	}
}
new ECF_Option_Required();
?>