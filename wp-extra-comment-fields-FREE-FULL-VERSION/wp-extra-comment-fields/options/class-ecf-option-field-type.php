<?php
/* An abstract class for providing list based options */
class ECF_Option_Field_Type extends ECF_Option {

	public function get_name() {
		return 'type';
	}

	public function get_full_name() {
		return 'Field Type';
	}

	public function get_description() {
		return 'The type of the field.';
	}

	public function get_default_value() {
		return 'text';
	}
	
	public function options_form_field( $ref, $field = null ) {
		$value = $this->get_value( $field );
		$html = sprintf( "<select name='%s[%s]'>\n", $this->get_name(), $ref );
		foreach ( ECF_Field_Type::get_types() as $name => $type ) {
			$html .= sprintf( "\t<option value='%s'%s>%s</option>\n",
				$name, selected( $name, $value, $false ),
				$type->get_description() );
		}
		$html .= "</select>\n";
		return $html;
	}

	public function get_value( $field ) {
		global $ecfdb;

		return $field ? $ecfdb->html_string( parent::get_value( $field ) )
			: $this->get_default_value();
	}

	public function priority() {
		return 98;
	}
}
new ECF_Option_Field_Type();
?>