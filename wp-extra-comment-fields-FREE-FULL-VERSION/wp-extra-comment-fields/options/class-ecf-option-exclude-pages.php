<?php
class ECF_Option_Exclude_Pages extends ECF_Option_List
	implements ECF_Visibility_Condition {
	
	public function get_name() {
		return 'exclude_pages';
	}

	public function get_full_name() {
		return 'Exclude Page / Post IDs';
	}

	public function get_description() {
		return 'Specify page or post IDs where the extra field is not to be '
			. 'displayed. For example, use in conjuction with category '
			. '(e.g. news) but exclude from one specific post? Or perhaps '
			. 'you\'d just like to exclude it from your home page for '
			. 'example? Seperate multiple IDs with a comma e.g. 4,9,356.';
	}

	public function get_default_value() {
		return '';
	}

	public function is_satisfied( $field ) {
		global $post;

		// The post id must NOT be in the list of excluded post IDs
		return ! in_array( $post->ID, $this->get_value( $field ) );
	}

	public function priority() {
		return 56;
	}
}
new ECF_Option_Exclude_Pages();
?>