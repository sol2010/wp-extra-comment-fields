<?php
class ECF_Option_Pages extends ECF_Option_List
	implements ECF_Visibility_Condition {
	
	public function get_name() {
		return 'pages';
	}

	public function get_full_name() {
		return 'Page / Post IDs';
	}

	public function get_description() {
		return 'Do you want to display the field only on specific pages or '
			. 'posts? For example, you may only want to show a specific field '
			. 'on just two pages and one post? Enter the IDs of these, '
			. 'seperated with a comma e.g. 4,9,356 or leave blank to display '
			. 'this field on all pages and posts. This can be used in '
			. 'conjunction with the category dropdown. '
			. '<a href="http://tinyurl.com/73gxlgj">'
			. '(How do I find post/page IDs?</a>).';
	}

	public function get_default_value() {
		return '';
	}

	public function is_satisfied( $field ) {
		global $post;

		$posts = $this->get_value( $field );
		// The post id must be in the array of valid post ids
		return '' == $posts[0] || in_array( $post->ID, $posts );
	}

	public function priority() {
		return 58;
	}
}
new ECF_Option_Pages();
?>