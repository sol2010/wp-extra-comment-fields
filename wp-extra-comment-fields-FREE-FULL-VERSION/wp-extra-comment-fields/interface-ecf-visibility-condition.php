<?php
/* An interface describing an option which can determine when the comment form
 * is displayed */
interface ECF_Visibility_Condition {
	/* Whether the condition has been met */
	public function is_satisfied( $field );
}
?>