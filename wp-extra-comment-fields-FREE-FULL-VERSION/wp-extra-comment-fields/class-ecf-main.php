<?php
require_once 'class-ecf-admin-notifications.php';
include_once 'class-ecf-admin.php';
include_once 'class-ecf-mail.php';

/* Main plugin class */
class ECF_Main {
	// The name of the plugin
	const PLUGIN_NAME = 'Extra Comment Fields';

	// Constructor initiaiating hooks and filters
	public function __construct() {
		//if ( $GLOBALS['ecf_plugin_API']->get_deliverables() ) {
			add_filter( 'comment_text', array( &$this, 'comment_text' ) );
			add_filter( 'thesis_comment_text',
				array( &$this, 'comment_text' ) );
			add_action( 'comment_form', array( &$this, 'comment_form' ) );
			add_action( 'comment_post', array( &$this, 'save_fields' ) );
		// }
		//add_action( 'admin_head', array( &$this, 'update_check' ) );
		add_action( 'wp_enqueue_scripts', array( &$this, 'scripts' ) );
		add_action( 'admin_init', array( &$this, 'admin_styles' ) );
		if ( isset( $GLOBALS['ecf_settings_global'] ) )
			add_filter( 'comment_form_default_fields',
				array( &$this, 'remove_disabled_comment_form_fields' ) );

		if ( class_exists( 'ECF_Admin' ) )
			new ECF_Admin();
		if ( class_exists( 'ECF_Mail' ) )
			new ECF_Mail();
	}

	// Include styles for admin page
	public function admin_styles() {
		wp_enqueue_style( 'stylesheet',
			plugins_url( 'admin-style.css', __FILE__ ), array(), ECF_VERSION );
	}

	// Make sure jQuery is included
	public function scripts() {
		wp_enqueue_script( 'jquery' );
	}

	// Display an appropriate update notification
	public function update_notification( $version = null ) {
		$msg = sprintf( 'A new version of %s %shas been released. '
			. '<a href="%s">Update Now...</a>', PLUGIN_NAME,
			$version ? "($version) ": '', ecf_plugin_data( 'update_url' ) );
		ECF_Admin_Notifications::message_updated( $msg );
	}

	// Check the plugin is at the latest version available
	// removed
	

	public function comment_text( $text ) {
		global $comment, $ecfdb, $ecf_settings_global;

		$data = $ecfdb->get_comment_data( $comment->comment_ID );

		$fields = '';
		foreach ( $data as $id => $value ) {
			$field = $ecfdb->get_field( $id );
			$field_type = ECF_field_type::get_type( $field->type );
			$value = is_array( $value )
				? array_map( 'esc_html', $value ) : esc_html( $value );
			if ( ECF_Option::get_option( 'public' )->get_value( $field )
				|| is_admin() )
				$fields .= $field_type->display_field( $id,
					$ecfdb->html_string( $field->name ), $value );
		}

		// Combine the fields with the comment text as specified in the
		// fields_display_position setting
		switch ( $ecf_settings_global->get_setting( 'fields_display_position' ) ) {
		case 'above':
			return "$fields\n$text";

		case 'below':
		default:
			return "$text\n$fields";
		}
	}

	// Seperate functions as closures were not supported before PHP 5.3.0

	/* Return cat_ID attribute */
	private static final function extract_cat_ID( $category ) {
		return $category->cat_ID;
	}

	public function comment_form() {
		global $ecfdb;

		echo '<div id="ecf_form_fields">';
		foreach ( $ecfdb->get_fields_by_ref() as $ref => $field ) {
			// Only display the form if all visibility conditions are satisfied
			if ( ECF_Option::are_visibility_conditions_satisfied( $field ) ) {
				$id = $ecfdb->unwrap_id( $ref );
				assert( 'filter_var($id, FILTER_VALIDATE_INT)' );
				printf( '<div id="ecf-form-field-%d" class="ecf-form-field">',
					$id );
				$type = ECF_Field_Type::get_type( $field->type );
				$type->form_field( $ref, $field );
				echo '</div>';
			}
		}
		echo '</div>';

		// JavaScript to correctly position the fields within the comment form
?>
		<script type="text/javascript">
		var id = 'form[id^=comment]';
		var commentform = jQuery(id)[0];
		var fields = jQuery(id + ' #ecf_form_fields')[0];
		var submit = jQuery(id + ' :submit')[0];
		while (submit.parentElement.nodeName != 'FORM')
			submit = submit.parentElement;
		var comment = jQuery(id + ' textarea')[0];
		var limit = 10;
		while (comment && comment.parentElement.nodeName != 'FORM'
			&& limit-- > 0)
			comment = comment.parentElement;
		if (commentform && fields && comment && submit) {
			commentform.appendChild(fields);
			commentform.appendChild(comment);
			commentform.appendChild(submit);
		}
		</script>
		<?php
	}

	// Extract the type of a field
	private static final function field_type( $field ) {
		return $field->type;
	}

	// Is an uploaded file an image
	public static final function is_image( $name, $mime_type ) {
		return preg_match( '/(jpeg|png|gif)/i', $mime_type )
			&& preg_match( '/^[^\\.]+\\.(jpeg|png|jpg|gif)$/i', $name );
	}

	// Retrieve the data from post for each field
	private static final function submitted_field_data( $ref, $field_type ) {
		if ( 'image' == $field_type ) {
			if ( array_key_exists( $ref, $_FILES )
				&& self::is_image( $_FILES[$ref]['name'],
					$_FILES[$ref]['type'] ) ) {
				$upload = wp_upload_bits( $_FILES[$ref]['name'], null,
					file_get_contents( $_FILES[$ref]['tmp_name'] ) );
				return $upload['url'];
			}
		} else if ( array_key_exists( $ref, $_POST ) ) {
				return $_POST[$ref];
			}
	}

	// Retrieve the user submitted data from post
	private function get_submitted_field_data() {
		global $ecfdb;

		$fields = $ecfdb->get_fields_by_ref();
		$refs = array_keys( $fields );
		$types = array_map( 'ECF_Main::field_type', $fields );

		return array_combine( $refs,
			array_map( 'ECF_Main::submitted_field_data', $refs, $types ) );
	}

	// Save the submitted field data in the comment data
	public function save_fields( $comment_id ) {
		global $ecfdb;

		$ecfdb->add_comment_data( $comment_id,
			$this->get_submitted_field_data() );
	}

	// Remove disabled default comment form fields
	public function remove_disabled_comment_form_fields( $original_fields ) {
		$enabled_fields = $GLOBALS['ecf_settings_global']->get_setting(
			'enabled_fields' );
		$fields = array();
		foreach ( $original_fields as $name => $field ) {
			if ( in_array( $name, $enabled_fields ) )
				$fields[$name] = $field;
		}
		return $fields;
	}
}
?>
