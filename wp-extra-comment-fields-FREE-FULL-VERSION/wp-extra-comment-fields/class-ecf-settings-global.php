<?php
require_once( 'class-ecf-admin-notifications.php' );

/**
* The included settings for the global settings page
*/
class ECF_Settings_Global
{
	// The name of the settings section for display
	const SETTINGS_SECTION_DISPLAY = 'ecf_settings_display';
	// The name of the settings section for the comment form
	const SETTINGS_SECTION_COMMENT_FORM = 'ecf_settings_comment_form';
	// The default settings
	private $defaults = array();

	/* Constructor -- register actions for adding settings */
	function __construct()
	{
		// Set the defaults
		$this->defaults['fields_display_position'] = 'below';
		$this->defaults['enabled_fields'] = array( 'author', 'email', 'url' );

		add_action( 'admin_init', array( &$this, 'register_settings' ) );
		add_action( 'admin_init',
			array( &$this, 'register_settings_display' ) );
		add_action( 'admin_init',
			array( &$this, 'register_settings_comment_form' ) );
	}

	/* Register settings */
	public function register_settings() {
		// Create the option if it does not exist
		if ( false === get_option( ECF_Admin::GLOBAL_SETTINGS_PAGE ) )
			add_option( ECF_Admin::GLOBAL_SETTINGS_PAGE );

		// Register the settings fields so WordPress handles POST
		register_setting( ECF_Admin::GLOBAL_SETTINGS_PAGE,
			ECF_Admin::GLOBAL_SETTINGS_PAGE );
	}

	/* Retrieve a given setting */
	public function get_setting( $name ) {
		$options = get_option( ECF_Admin::GLOBAL_SETTINGS_PAGE );
		return ( isset( $options[$name] ) && ! is_null( $options[$name] ) )
			? $options[$name] : $this->defaults[$name];
	}

	/* Register settings in the display section */
	public function register_settings_display() {
		add_settings_section( self::SETTINGS_SECTION_DISPLAY,
			'Display / Appearance Settings',
			array( &$this, 'settings_display_description' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE );

		// Add the setting for the position of the comment fields
		add_settings_field( 'fields_display_position',
			'Fields Display Position',
			array( &$this, 'settings_field_select' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE, self::SETTINGS_SECTION_DISPLAY,
			array(
				'id' => 'fields_display_position',
				'options' => array(
					'above' => 'Above Comment',
					'below' => 'Below Comment'
					)
				) );
	}

	/* The description of the display settings section */
	public function settings_display_description() {
		?>
		<p>Settings relating to how the fields will be displayed to your
			users.</p>
		<?php
	}

	/* Register settings in the comment form section */
	public function register_settings_comment_form() {
		add_settings_section( self::SETTINGS_SECTION_COMMENT_FORM,
			'Comment Form Settings',
			array( &$this, 'settings_display_description' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE );

		add_settings_field( 'enabled_fields', 'Display Standard Fields',
			array( &$this, 'settings_field_enabled_fields' ),
			ECF_Admin::GLOBAL_SETTINGS_PAGE,
			self::SETTINGS_SECTION_COMMENT_FORM );
	}

	/* The description of the display settings section */
	public function settings_comment_form_description() {
		?>
		<p>Which forms are displayed on the comment form?</p>
		<?php
	}

	/* Display a placeholder for an unavailable option */
	public function settings_field_unavailable( $args ) {
		ECF_Admin_Notifications::unavailable( $args['available_in'] );
	}

	public function settings_field_enabled_fields( $args ) {
		$enabled_fields = $this->get_setting( 'enabled_fields' );
		echo '<fieldset>';
		$this->settings_field_checkbox( array(
			'id' => 'author_field_enabled',
			'name' => 'enabled_fields',
			'value' => 'author',
			'label' => 'Author Name',
			'checked' => in_array( 'author', $enabled_fields )
		) );
		echo '<br />';
		$this->settings_field_checkbox( array(
			'id' => 'email_field_enabled',
			'name' => 'enabled_fields',
			'value' => 'email',
			'label' => 'Email Address',
			'checked' => in_array( 'email', $enabled_fields )
		) );
		echo '<br />';
		$this->settings_field_checkbox( array(
			'id' => 'url_field_enabled',
			'name' => 'enabled_fields',
			'value' => 'url',
			'label' => 'Website URL',
			'checked' => in_array( 'url', $enabled_fields )
		) );
		echo '</fieldset>';
	}

	/* Display a dropdown selection field for an option */
	public function settings_field_select( $args ) {
		$id = $args['id'];
		$name = sprintf( '%s[%s]', ECF_Admin::GLOBAL_SETTINGS_PAGE, $id );
		$value = $this->get_setting( $id );
		echo "<select id='$id' name='$name' type='select'>";
		foreach ( $args['options'] as $option => $text ) {
			printf( "\t<option value='$option'%s>$text</option>\n",
				selected( $option, $value, false ) );
		}
		echo '</select>';
	}

	/* Display a checkbox field for an option */
	public function settings_field_checkbox( $args ) {
		$id = $args['id'];
		$name = sprintf( '%s[%s][]', ECF_Admin::GLOBAL_SETTINGS_PAGE, $args['name'] );
		$value = isset( $args['value'] ) ? $args['value'] : true;
		$label = isset( $args['label'] ) ? $args['label'] : null;
		$checked = isset( $args['checked'] )
			? $args['checked'] : $this->get_setting( $id );
		if ( ! is_null( $label ) ) echo "<label for='$id'>\n";
		printf( '<input type="checkbox" id="%s" name="%s" value="%s"%s>',
			$id, $name, $value, checked( $checked, true, false ) );
		if ( ! is_null( $label ) ) echo "\n$label\n</label>";
	}

}
?>